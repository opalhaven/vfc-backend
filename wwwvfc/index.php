<?php
##
## Rewrite rules for IIS for EPI package
##    https://github.com/jmathai/epiphany/blob/master/docs/Route.markdown
##
	include_once 'src/Epi.php';
	require ( "src/sphinxapi.php" );
##	Epi::setPath('base', 'src'); ?? maybe be useful later
    $debug = true;
	Epi::setSetting('exceptions', !$debug);
	Epi::init('route');
	Epi::init('database');
	
	getRoute()->get('/API', 'ping');
	getRoute()->get("/API/AudioMessageDump", "getAudioMessagesDump");
	getRoute()->get("/API/AudioMessage", "getAudioMessages");
	getRoute()->get("/API/search", "searchGeneral");
	getRoute()->run();

    function addHeaders() 
	{
		header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');## required for AJAX
		header('Access-Control-Max-Age: 1000');## required for AJAX
		header('Access-Control-Allow-Credentials: false');## required for AJAX
		$http_origin            = $_SERVER['HTTP_ORIGIN']; ## required for AJAX 
		@header("Access-Control-Allow-Origin: " . $http_origin);
	}
	function searchGeneral()
	{
	    addHeaders();
		$results = new SearchResult();
		$results->search();
		echo json_encode($results);
	}
	function getAudioMessages()
	{
	    addHeaders();
	    $results = new AudioMessageResult();
		$results->getAudioMessagesFilteredSearch();
		$results->getAudioMessagesFilterSpeakers();
		$results->getAudioMessagesFilterDate();
		$results->getAudioMessagesFilterLanguage();
		$results->getAudioMessagesFilterPlace();
		echo json_encode($results);
	}
	function getAudioMessagesDump()
	{
		try {
			addHeaders();
			$results = new AudioMessageResult();
			echo $results->dump();
		}
		catch ( Exception $e ) {
            echo 'getAudioMessageDump Caught exception: ',  $e->getMessage(), "\n"; 
		}
	}
	function ping()
	{
	    addHeaders();
		try {
			$objDateTime = new DateTime('NOW');
			echo 'v0.1|' . $objDateTime->format('c');
		}
		catch ( Exception $e ) {
            echo 'ping Caught exception: ',  $e->getMessage(), "\n"; 
		}
	}
	abstract class DatabaseOp {
		function __construct() {
		    if ( !empty($this->authUser) &&  (empty($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != $this->authUser) ) {
				header('HTTP/1.1 403 Forbidden');
				exit;
			}
		    if ( !empty($this->authPwd) &&  (empty($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_PW'] != $this->authPwd) ) {
				header('HTTP/1.1 403 Forbidden');
				exit;
			}

			$databaseName = 'VFC_Perry';
			$databaseServer = 'us-cdbr-azure-west-b.cleardb.com';
			$databaseUser = 'blah blah';
			$databasePwd = 'blah blah';

			EpiDatabase::employ('mysql', $databaseName, $databaseServer, $databaseUser, $databasePwd);
		}
	}
	class SearchResult extends DatabaseOp {
		public $searchResult = array();
		public $logDataDebug = '';
		function __construct() {
			parent::__construct();
		}

		public function search() {
		    $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://voicesforchrist.org/welcome/search?term=john'  );
			$header[] = 'X-Requested-With: XMLHttpRequest';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
			$output = curl_exec($ch);
			curl_close($ch);      
			$array = json_decode($output);
			$this->searchResult = array();
			foreach ( $array as $item ) {											 
				 array_push($this->searchResult, $item);
			}
		}
	}

	class AudioMessageResult extends DatabaseOp
	{
	    public $sEcho = "1";

	    public $iTotalRecords = "100";
	    public $iTotalDisplayRecords = "100";

		public $aaData = array();  # data tables results
		public $fsData = array();  # filtered speaker 
		public $fdData = array();  # filtered date
		public $flData = array();  # filtered language
		public $fpData = array();  # filtered place
		public $logData = '';
	    private $query_rowNumber;
		private $query_pageCount;
		private $query_sSearch;
		private $filter_speakerId = null;
		private $filter_placeId = null;
		private $filter_eventYear = null;
		private $filter_languageId = null;
		function __construct() {
			parent::__construct();

			$this->sEcho        = $_GET['sEcho'];

			$this->query_rowNumber    = is_null($_GET['iDisplayStart']) ? 0 : $_GET['iDisplayStart'];
			$this->query_pageCount    = is_null($_GET['iDisplayLength']) ? 10 : $_GET['iDisplayLength'];
			$this->query_sSearch      = empty($_GET['sSearch']) ? null : $_GET['sSearch'];     

		    $this->filter_placeId     = empty($_GET['PID'])    ? null : explode(',', $_GET['PID'] );
		    $this->filter_speakerId   = empty($_GET['SID'])  ? null : explode(',', $_GET['SID'] );
		    $this->filter_languageId  = empty($_GET['LID']) ? null : explode(',', $_GET['LID'] );
		    $this->filter_eventYear   = empty($_GET['YEAR']) ? null : explode(',', $_GET['YEAR'] );
		}
	    private function getAttributeIdsFromSphinx( $sphinx_attr ) {
			
			$sphinxClient = new SphinxClient ();
			$sphinxClient->SetServer ( "opalsphinxse.cloudapp.net", 3312 );
			$sphinxClient->SetMatchMode ( SPH_MATCH_ALL );
			$sphinxClient->SetGroupBy( $sphinx_attr, SPH_GROUPBY_ATTR, '@count DESC');
			## $sphinxClient->SetConnectTimeout ( 1 );  
			$sphinxClient->SetLimits( 0, 5 ); 
			
			if ( count($this->filter_placeId) )  $sphinxClient->SetFilter ( 'place_id', $this->filter_placeId );
			if ( count($this->filter_speakerId) )  $sphinxClient->SetFilter ( 'speaker_id', $this->filter_speakerId );
			if ( count($this->filter_languageId) )  $sphinxClient->SetFilter ( 'language_id', $this->filter_languageId );
			if ( count($this->filter_eventYear) )  $sphinxClient->SetFilter ( 'event_year', $this->filter_eventYear );

			$queryRes = $sphinxClient->Query ( $this->query_sSearch, 'audio_source' );

			$result = null;
			if ( !$queryRes ) {
				echo "Query failed: " . $sphinxClient->GetLastError() . ".\n";
			} else {
				if ( $sphinxClient->GetLastWarning() )
					echo "WARNING: " . $sphinxClient->GetLastWarning() . "\n\n";
				if ( ! empty ($queryRes["matches"]) ) { 
					foreach ( $queryRes["matches"] as $attr_id => $blahblah ) {
					    $sphinxGroupBy = $queryRes["matches"][$attr_id]["attrs"]['@groupby'];
						$result[$sphinxGroupBy] = $queryRes["matches"][$attr_id]["attrs"]['@count'];
					}
				}
			}
			return $result;
		}
	    private function getAudioIdsFromSphinx() {
			$result = null;
			$sphinxClient = new SphinxClient ();
			$sphinxClient->SetServer ( "opalsphinxse.cloudapp.net", 3312 );
			$sphinxClient->SetMatchMode ( SPH_MATCH_ALL );
			## $sphinxClient->SetConnectTimeout ( 1 ); 
			$sphinxClient->SetLimits( $this->query_rowNumber, $this->query_pageCount );

			if ( count($this->filter_placeId) )  $sphinxClient->SetFilter ( 'place_id', $this->filter_placeId );
			if ( count($this->filter_speakerId) )  $sphinxClient->SetFilter ( 'speaker_id', $this->filter_speakerId );
			if ( count($this->filter_languageId) )  $sphinxClient->SetFilter ( 'language_id', $this->filter_languageId );
			if ( count($this->filter_eventYear) )  $sphinxClient->SetFilter ( 'event_year', $this->filter_eventYear );

			$queryRes = $sphinxClient->Query ( $this->query_sSearch, 'audio_source' );

			if ( !$queryRes )  {
				echo "Query failed: " . $sphinxClient->GetLastError() . ".\n";
			} else {
				if ( $sphinxClient->GetLastWarning() )
					echo "WARNING: " . $sphinxClient->GetLastWarning() . "\n\n";

				if ( ! empty ($queryRes['total_found']) ) { 
					$this->iTotalDisplayRecords = $queryRes['total_found'];
				} 

				if ( ! empty ($queryRes["matches"]) ) { 
					foreach ( $queryRes["matches"] as $id_audio => $blahblah ) {
						if ( $result ) $result .= ',';
						$result .= $id_audio;
					}
				}
			}
			return $result;
		}
	    public function dump() {
			return 'DUMP AudioMessage GET Params'
			. '<p>'
			. '|QUERY rowNumber:' . $this->query_rowNumber
			. '|QUERY pageCount:' . $this->query_pageCount
			. '|QUERY sSearch:' . $this->query_sSearch
			. '|<p>'
		    . '|FILTER speaker_id:' . implode(",",$this->filter_speakerId)
		    . '|FILTER place_id:' . implode(",", $this->filter_placeId)
		    . '|FILTER eventYear:' . implode(",", $this->filter_eventYear)
		    . '|FILTER language_id:' . implode(",", $this->filter_languageId);
		}
		public function getAudioMessagesFilterPlace() {
			$itemAttr = 'place_id';
			$itemCountArray = $this->getAttributeIdsFromSphinx( $itemAttr );
			$itemIds = null;
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				if ( $itemIds ) $itemIds .= ',';
				$itemIds .= $itemIdKey;
			}
			if ( !is_null($itemIds) ) {
				$results = getDatabase()->all( 
						 'SELECT \'Not Available\' AS \'EventPlace\', \'0\' as \'place_id\'
						 UNION SELECT PLACE.name AS \'EventPlace\', PLACE.id as \'place_id\'
						 FROM places AS PLACE 
						 WHERE PLACE.id in ( ' . $itemIds . ' ) ', array());
			}
			# 
			# create results set by combining SPHINX results w/ names from database
			# 
			$this->fpData = array();
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				foreach( $results as $resultsArray ) {
					if ( $itemIdKey == $resultsArray[$itemAttr] ) {
						$resultsArray['PLACECOUNT'] = $sphinxCount;
						$resultsArray['place_id']   = $itemIdKey;
						$resultsArray['EventPlace'] = $resultsArray['EventPlace'];
						array_push( $this->fpData, $resultsArray );
						break;
					}
				}
			}
		}
		public function getAudioMessagesFilterLanguage() {
			$itemAttr = 'language_id';
			$itemCountArray = $this->getAttributeIdsFromSphinx( $itemAttr );
			$itemIds = null;
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				if ( $itemIds ) $itemIds .= ',';
				$itemIds .= $itemIdKey;
			}
			
			if ( $itemIds ) {
				$results = getDatabase()->all( 
						'SELECT LANG.name AS \'Language\',
								LANG.id AS \'language_id\'
						FROM languages AS LANG
						WHERE LANG.id in ( ' . $itemIds . ' ) ', array());
			}

			# 
			# create results set by combining SPHINX results w/ names from database
			# 
			$this->flData = array();
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				foreach( $results as $resultsArray ) {
					if ( $itemIdKey == $resultsArray[$itemAttr] ) {
						$resultsArray['LANGCOUNT'] = $sphinxCount;
						$resultsArray['language_id']   = $itemIdKey;
						$resultsArray['Language'] = $resultsArray['Language'];
						array_push( $this->flData, $resultsArray );
						break;
					}
				}
			}
		}
		public function getAudioMessagesFilterSpeakers() {
		    $itemAttr = 'speaker_id';
			$itemCountArray = $this->getAttributeIdsFromSphinx( $itemAttr );
			$itemIds = null;
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				if ( $itemIds ) $itemIds .= ',';
				$itemIds .= $itemIdKey;
			}
			
			if ( $itemIds ) {
				$results = getDatabase()->all( 
						'SELECT SPEAKER.last_name AS \'SpeakerLastName\',
								SPEAKER.first_name AS \'SpeakerFirstName\',
								SPEAKER.id as \'speaker_id\'
					FROM speakers AS SPEAKER 
				    WHERE SPEAKER.id in ( ' . $itemIds . ' ) ', array());
			}
			# 
			# create results set by combining SPHINX results w/ names from database
			# 
			$this->fsData = array();
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
				foreach( $results as $resultsArray ) {
					if ( $itemIdKey == $resultsArray[$itemAttr] ) {
						$resultsArray['SPEAKERCOUNT'] = $sphinxCount;
						$resultsArray['speaker_id']   = $itemIdKey;
						$resultsArray['SpeakerLastName'] = $resultsArray['SpeakerLastName'];
						$resultsArray['SpeakerFirstName'] = $resultsArray['SpeakerFirstName'];
						array_push( $this->fsData, $resultsArray );
						break;
					}
				}
			}
		}
		public function getAudioMessagesFilterDate() { 
		    $itemAttr = 'event_year';
			$itemCountArray = $this->getAttributeIdsFromSphinx( $itemAttr );
			$this->fdData = array();
			$resultsArray = null;
			foreach ( $itemCountArray as $itemIdKey => $sphinxCount ) {
			    $resultsArray['EventDate'] = $itemIdKey;
				$resultsArray['YEARCOUNT'] = $sphinxCount;
				array_push( $this->fdData, $resultsArray );
			}
		}
		
		public function getAudioMessagesFilteredSearch() {
            $audioIds = $this->getAudioIdsFromSphinx();
			if ( ! empty( $audioIds ) ) {
				$this->aaData = getDatabase()->all( 
						'SELECT AUDIO.id AS \'ID\',
								CONCAT( AUDIO.title, \' ~ \',  AUDIO.subj ) AS \'Title\',
								IF(MONTH(AUDIO.event_date)=1 and DAY(AUDIO.event_date)=1, YEAR(AUDIO.event_date), DATE_FORMAT(AUDIO.event_date,\'%Y-%m-%e\')) AS \'EventDate\',
								SPEAKER.last_name AS \'SpeakerLastName\',
								SPEAKER.first_name AS \'SpeakerFirstName\',
								SPEAKER.id as \'speaker_id\',
								PLACE.name AS \'EventPlace\',
								PLACE.id AS \'place_id\',
								LANG.name AS \'Language\',
								LANG.id AS \'language_id\'
						 FROM audio_messages AS AUDIO                      
						 JOIN speakers AS SPEAKER ON SPEAKER.id = AUDIO.speaker_id  
						 LEFT join places AS PLACE ON PLACE.id = AUDIO.place_id 
						 JOIN languages AS LANG ON LANG.id = AUDIO.language_id '
						 . 'WHERE AUDIO.id IN ( ' . $audioIds . ' )  ORDER BY AUDIO.id ', array());

			}

			$result = getDatabase()->one( 
					'SELECT count(*) AS \'COUNT\'
					 FROM audio_messages AS AUDIO', array());
			$this->iTotalRecords = $result['COUNT'];

		}
	}
?>
